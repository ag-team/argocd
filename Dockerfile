FROM openjdk:8-jdk-alpine
# FROM maven:3.6.3-jdk-11
# COPY pom.xml .
# RUN mvn dependency:go-offline
# COPY . .
VOLUME /tmp
ADD target/my-app.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]